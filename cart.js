/* =========================================================   
Global Variables 
========================================================= */
var bakerShopping = bakerShopping || {},
productName = '.product-name',
productPrice = '.product-price',
productDesc = '.product-desc',
addProduct = '.product-add-to-cart',
cartIcon = '#cart-icon',
cartDisplay = '#cart-view',
cartItems = JSON.parse(localStorage.getItem('cart'));

/* =========================================================   
Function that will add a product to our shopping cart. 
Uses localStorage to save the data to browser storage. 
========================================================= */

bakerShopping.addToCart = function() {
  var cart = JSON.parse(localStorage.getItem('cart')) || [];
  
  $(addProduct).on('click', function() {
    var product = {};
    
    product.id = $(this).parent('.product').attr('data-product-id');
    product.price = parseFloat($(this).siblings(productPrice).text());
    product.name = $(this).siblings(productName).text();
  
    cart.push(product);
    localStorage.setItem( 'cart', JSON.stringify( cart ));
    
    $(cartIcon).find('span').text(cart.length);

  });
}

/* ========================================================= 
bakerShopping.updateCartQuanitity 
Function that updates a products quauntity in our cart. 
========================================================= */

bakerShopping.updateCartQuanitity = function() {
  $('.item-count').on('change', function(){
    console.log($(this).val());

    // localStorage.setItem('cart', )
  });
}

/* =========================================================   
Function that removes the entire product from cart. 
When a user clicks delete it removes all instances 
of that item. 
========================================================= */

bakerShopping.removeItem = function() {
  items = JSON.parse(localStorage.getItem('cart'));
  $('a.remove').on('click', function(){
    elId = $(this).parent('td').parent('tr').attr('data-id');
    updatedData = [];
    $.each(items, function(k, v) {
      if(items[k].id !== elId) {
        updatedData.push(items[k]);
      }
    });

    localStorage.setItem('cart', JSON.stringify(updatedData));
    console.log('stop');
    $(this).closest('tr').remove();
    
    var sum = 0;
    $('.line-total').each(function() {
        sum += Number($(this).text());
    });
    
    $('.total').text(sum.toFixed(2));
  });
 
};

/* =====   
Begin - bakerShopping.cartView 
Function that handles the display of the cart view. Uses
data saved in the localStorage and outputs it. using a Map() 
===== */
bakerShopping.cartView = function(arr) {
  
  var map = new Map();
  var filtered = [];
  
  $.each(arr, function(k, v) {
    var index = map.get(v.id);
    if (index === undefined) {
      v.count = 1;
      map.set(v.id, filtered.push(v) - 1);
      return;
    }
    filtered[index].count = ((filtered[index].count + 1) || 1);
  });  

  $.each(filtered, function(k, v){
    lineTotal = v.price * v.count;
    $(cartDisplay).append('<tr data-id="'+ v.id +'"><td>' +  v.name 
    + '</td><td class="price">' + v.price.toFixed(2) 
    + '</td><td class="quantity">' 
    + '<input type="number" class="item-count" value="' + v.count + '"/>' 
    + '</td><td class="line-total">'+ lineTotal.toFixed(2) +'</td><td>' 
    + '<a class="remove" href="#">Delete</a></td></tr>');
   });

  var sum = 0;
  $('.line-total').each(function() {
      sum += Number($(this).text());
  });
  $(cartDisplay).append('<tr><td>Sub Total</td><td></td><td></td>'
  +'<td class="total">' +  sum.toFixed(2) + '</td></tr>');

  taxableAmmount = parseFloat(sum.toFixed(2)) * parseFloat($('#cart-view').attr('data-tax-rate'));
  totalWithTax = taxableAmmount + parseFloat(sum.toFixed(2));
  console.log(totalWithTax.toFixed(2));
}

/* =====   
Initialize all the functions 
===== */
$(document).ready(function(){
  saveToCart = new bakerShopping.addToCart();
  viewCart = new bakerShopping.cartView(cartItems);
  removeItem = new bakerShopping.removeItem();
  cartUpdate = new bakerShopping.updateCartQuanitity();
});